#!/usr/bin/env python3

__author__ = "Carlo van Buiten"

import sys
import argparse
import math

codons = {
    'ATA': 'I', 'ATC': 'I', 'ATT': 'I', 'ATG': 'M',
    'ACA': 'T', 'ACC': 'T', 'ACG': 'T', 'ACT': 'T',
    'AAC': 'N', 'AAT': 'N', 'AAA': 'K', 'AAG': 'K',
    'AGC': 'S', 'AGT': 'S', 'AGA': 'R', 'AGG': 'R',
    'CTA': 'L', 'CTC': 'L', 'CTG': 'L', 'CTT': 'L',
    'CCA': 'P', 'CCC': 'P', 'CCG': 'P', 'CCT': 'P',
    'CAC': 'H', 'CAT': 'H', 'CAA': 'Q', 'CAG': 'Q',
    'CGA': 'R', 'CGC': 'R', 'CGG': 'R', 'CGT': 'R',
    'GTA': 'V', 'GTC': 'V', 'GTG': 'V', 'GTT': 'V',
    'GCA': 'A', 'GCC': 'A', 'GCG': 'A', 'GCT': 'A',
    'GAC': 'D', 'GAT': 'D', 'GAA': 'E', 'GAG': 'E',
    'GGA': 'G', 'GGC': 'G', 'GGG': 'G', 'GGT': 'G',
    'TCA': 'S', 'TCC': 'S', 'TCG': 'S', 'TCT': 'S',
    'TTC': 'F', 'TTT': 'F', 'TTA': 'L', 'TTG': 'L',
    'TAC': 'Y', 'TAT': 'Y', 'TAA': '_', 'TAG': '_',
    'TGC': 'C', 'TGT': 'C', 'TGA': '_', 'TGG': 'W'}


def argument_parser():
    """
    This function creates an 'argparse' parser and adds arguments.
        :return parser.parse_args(): - The parser arguments
    """
    # Create parser with a description and usage
    parser = argparse.ArgumentParser(
        description='Determines conservation.',
        usage='python3.7 SNP.py -n [SNP] -p [Gene position] -m [MSA]'
    )

    # Add arguments:
    parser.add_argument('-n', '--snp', type=str, metavar='', required=True,
                        help='SNP Nucleotide')
    parser.add_argument('-p', '--position', type=int, metavar='', required=True,
                        help='Gene position')
    parser.add_argument('-m', '--msa', type=str, metavar='', required=True,
                        help='Multiple sequence alignment')

    return parser.parse_args()


def find_amino(position, snp):
    """
    Finds the amino acids/triplets associated with the SNP.
    :param position: Position of the mutation.
    :param snp: The new nucleotide.
    :return:
    """
    sequence_file = open("human_ubiquitin_nucleotide.fasta")
    sequence = ""
    for line in sequence_file:
        if not line.startswith(">"):
            sequence += line.strip()
    og_triplet = sequence[position - (position % 3):position + (3 - (position % 3))]
    new_triplet = list(og_triplet)
    new_triplet[(position % 3)] = snp
    new_triplet = ''.join(new_triplet)

    og_acid = codons[og_triplet]
    new_acid = codons[new_triplet]
    return og_acid, new_acid


def mutation_effect(old, new):
    """
    Tests to see whether the mutation resulted in a different amino acid.
    :param old: The old amino acid.
    :param new: The new amino acid.
    :return:
    """
    if old != new:
        return True
    else:
        return False


def msa_reader(msa):
    """
    Reads in the MSA file.
    :param msa: The path to the MSA file.
    :return:
    """
    msa_file = open(msa)
    score = ""
    for line in msa_file:
        if line.startswith(" "):
            score += line[20:].replace("\n", "")
    return score


def determine_severity(position, msa):
    """
    Gives feedback on mutation severity.
    :param position: Position of the mutation.
    :param msa: String containing the MSA scores.
    :return:
    """
    if msa[position] == "*":
        print("Severe: Mutation in highly conserved position.")
    elif msa[position] == ":":
        print("Potentially severe: Mutation in moderately conserved position.")
    elif msa[position] == ".":
        print("Unlikely severe: Mutation in mildly conserved position.")
    elif msa[position] == " ":
        print("Inconsequential: Mutation in unconserved position.")

    return 0


def main(args):
    """
    :param args:
    :return:
    """
    og_acid, new_acid = find_amino(args.position, args.snp)
    if mutation_effect(og_acid, new_acid):
        acid_position = math.ceil(args.position / 3 - 1)
        msa = msa_reader(args.msa)
        determine_severity(acid_position, msa)
    else:
        print("Inconsequential: Mutation has no effect.")

    return 0


if __name__ == "__main__":
    sys.exit(main(argument_parser()))
